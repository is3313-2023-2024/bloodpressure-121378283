/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simonwoodworth.bloodpressure;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author simon
 */

public class BloodPressureCalculatorTest {

    private static final double DELTA = 1e-15;
    
    @Test
    public void testCalculateMAP_NormalValues() {
        assertEquals(93.33333333333333, BloodPressureCalculator.calculateMAP(120, 80), DELTA);
    }

    @Test
    public void testCalculateMAP_EdgeLowValues() {
        assertEquals(46.666666666666667, BloodPressureCalculator.calculateMAP(60, 40), DELTA);
    }

    @Test
    public void testCalculateMAP_EdgeHighValues() {
        assertEquals(166.66666666666667, BloodPressureCalculator.calculateMAP(200, 150), DELTA);
    }

    @Test
    public void testCalculateMAP_SameSystolicDiastolic() {
        assertEquals(100, BloodPressureCalculator.calculateMAP(100, 100), DELTA);
    }

    @Test
    public void testCalculateMAP_NegativeSystolic() {
        assertThrows(IllegalArgumentException.class,
            ()->{
                BloodPressureCalculator.calculateMAP(-120, 80);
            });        
    }

    @Test
    public void testCalculateMAP_NegativeDiastolic() {
        assertThrows(IllegalArgumentException.class,
            ()->{
                BloodPressureCalculator.calculateMAP(120, -80);
            });  
    }

}
